package ru.fosslabs.kfunews.fragments;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.fosslabs.kfunews.R;
import ru.fosslabs.kfunews.adapter.NewsListAdapter;
import ru.fosslabs.kfunews.database.DatabaseHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class ListNewsFragment extends Fragment {

	private ListView kfulist; // listview for news
	private ProgressBar progressBar; // progressbar if internet connect low
	LinearLayout linearNews;
	private PullToRefreshListView mPullRefreshListView; // pulltorefresh
														// listview for post
	private DatabaseHelper dbHelper; //database helper
	private SQLiteDatabase sdb;
	ArrayList<String> titlelist; // list of titles
	ArrayList<String> datelist; // list of dates
	ArrayList<String> textlist; // list of news text
	ArrayList<String> linklist; // list of links
	ArrayList<String> photolist; // list of photos
	ArrayList<String> photo_path; //list of photo src
	
	/*
	* check connection to the internet
	* check connection to wi-fi or mobile internet
	*/

	public Map<String, String> getConnectionDetails() {
		Map<String, String> networkDetails = new HashMap<String, String>();
		try {
			ConnectivityManager connectivityManager = (ConnectivityManager) getActivity()
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo wifiNetwork = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (wifiNetwork != null && wifiNetwork.isConnected()) {

				networkDetails.put("Type", wifiNetwork.getTypeName());
				networkDetails.put("Sub type", wifiNetwork.getSubtypeName());
				networkDetails.put("State", wifiNetwork.getState().name());
			}

			NetworkInfo mobileNetwork = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (mobileNetwork != null && mobileNetwork.isConnected()) {
				networkDetails.put("Type", mobileNetwork.getTypeName());
				networkDetails.put("Sub type", mobileNetwork.getSubtypeName());
				networkDetails.put("State", mobileNetwork.getState().name());
				if (mobileNetwork.isRoaming()) {
					networkDetails.put("Roming", "YES");
				} else {
					networkDetails.put("Roming", "NO");
				}
			}
		} catch (Exception e) {
			networkDetails.put("Status", e.getMessage());
		}
		return networkDetails;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_news, container, false);
		setHasOptionsMenu(true);
		progressBar = (ProgressBar) view.findViewById(R.id.progressbarnews);
		linearNews = (LinearLayout) view.findViewById(R.id.linearnewslayout);
		mPullRefreshListView = (PullToRefreshListView) view
				.findViewById(R.id.pull_refresh_list2);
		titlelist = new ArrayList<String>();
		datelist = new ArrayList<String>();
		textlist = new ArrayList<String>();
		linklist = new ArrayList<String>();
		photolist = new ArrayList<String>();
		photo_path = new ArrayList<String>();
		dbHelper = new DatabaseHelper(getActivity(), "newsdatabase.db", null, 1);
		sdb = dbHelper.getWritableDatabase();
		Log.d("FRAGMENT", "is working");
		// Set a listener to be invoked when the list should be refreshed.
		mPullRefreshListView
				.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(
							PullToRefreshBase<ListView> refreshView) {
						String label = DateUtils.formatDateTime(getActivity()
								.getApplicationContext(), System
								.currentTimeMillis(),
								DateUtils.FORMAT_SHOW_TIME
										| DateUtils.FORMAT_SHOW_DATE
										| DateUtils.FORMAT_ABBREV_ALL);

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy()
								.setLastUpdatedLabel(label);

						// Do work to refresh the list here.
						// new GetDataTask().execute();
					}
				});
		// Add an end-of-list listener
		mPullRefreshListView
				.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

					@Override
					public void onLastItemVisible() {

					}
				});
		Map<String, String> networkDetails = getConnectionDetails();
		if (networkDetails.isEmpty()) {
			// do only if internet connection is null
			Toast.makeText(getActivity().getApplicationContext(),
					"Нет интернет подключения", Toast.LENGTH_LONG).show();
			if (sdb != null) {
				Cursor cursor = sdb.query("news", null, null, null, null, null,
						null);
				Log.d("Cursor", "is null " + (cursor == null));
				cursor.moveToFirst();
				Log.d("Cursor", "is first " + (cursor.moveToFirst()));
				if (cursor.moveToFirst()) {
					Log.d("Cursor", "is null " + (cursor == null));
					int idColIndex = cursor.getColumnIndex("_id");
					int dateColIndex = cursor.getColumnIndex("date");
					int newsColIndex = cursor.getColumnIndex("news");
					int textColIndex = cursor.getColumnIndex("text");
					int photo_pathCol = cursor.getColumnIndex("photo_path");
					do {
						// получаем значения по номерам столбцов и пишем все в
						// лог
						Log.d("DB",
								"ID = " + cursor.getInt(idColIndex)
										+ ", date = "
										+ cursor.getString(dateColIndex)
										+ ", news = "
										+ cursor.getString(newsColIndex)
										+ ", photo = "
										+ cursor.getString(photo_pathCol));
						// переход на следующую строку
						// а если следующей нет (текущая - последняя), то false
						// - выходим из цикла
						titlelist.add(cursor.getString(newsColIndex));
						textlist.add(cursor.getString(textColIndex));
						datelist.add(cursor.getString(dateColIndex));
						photolist.add(cursor.getString(photo_pathCol));
					} while (cursor.moveToNext());
					linearNews.setVisibility(View.VISIBLE);
					progressBar.setVisibility(View.INVISIBLE);
					progressBar.setProgress(100);
					Log.d("Photo", "photolist = " + photolist);
					kfulist = mPullRefreshListView.getRefreshableView();
					NewsListAdapter newsadapter = new NewsListAdapter(
							getActivity(), titlelist, photolist, textlist,
							datelist, false);
					kfulist.setAdapter(newsadapter);
					kfulist = mPullRefreshListView.getRefreshableView();
					registerForContextMenu(kfulist);
				} else
					Log.d("DB", "0 rows");
				cursor.close();
			}
		} else {
			new getNews().execute();
			Log.d("Program", "program is working on create view");
		}
		return view;
	}

	class getNews extends AsyncTask<Void, Void, String> {

		ContentValues newValues;
		Elements el;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			linearNews.setVisibility(View.INVISIBLE);
			progressBar.setVisibility(View.VISIBLE);
			progressBar.setProgress(0);
		}

		@Override
		protected String doInBackground(Void... params) {
			newValues = new ContentValues();
			Log.d("Parser", "in background");
			Document doc = null;
			try {
				doc = Jsoup
						.connect("http://kpfu.ru/main_page?p_sub=12&p_type=2")
						.timeout(10 * 1000).get();
				Elements el1 = doc.select("div");
				for (Element elem : el1) {
					if (elem.attr("id").equals("news_list2")) {
						el = elem.getElementsByTag("img");
						for (Element elem2 : el) {
							Log.d("PHOTO", "SRC = " + elem2.absUrl("src"));
							photolist.add(elem2.absUrl("src"));
						}
						el = elem.getElementsByTag("span");
						for (Element ele : el) {
							Log.d("TITLE", "TITLE = " + ele.text());
							titlelist.add(ele.text());
						}
						el = elem.getElementsByTag("a");
						for (Element ele1 : el) {
							if (ele1.attr("class").equals("vn_new_date")) {
								Log.d("DATE", "DATE = " + ele1.text());
								Log.d("URL", "URL = " + ele1.attr("href"));
								datelist.add(ele1.text());
								linklist.add(ele1.attr("href"));
							}
						}
						el = elem.getElementsByTag("div");
						for (Element ele1 : el) {
							if (ele1.attr("align").equals("justify")) {
								Log.d("TEXT", "TEXT = " + ele1.text());
								textlist.add(ele1.text());
							}
						}
					}
				}
				if (photolist.size() < 10) {
					if (photolist.size() == 8) {
						photolist.add(5, "11");
						photolist.add(9, "11");
					} else {
						photolist.add(6, "11");
					}
				}
				SaveImage(photolist, photo_path);
				Log.d("PHOTO_PATH", "path = " + photo_path + " size = "
						+ photo_path.size());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (titlelist.size() > 0) {
				linearNews.setVisibility(View.VISIBLE);
				progressBar.setVisibility(View.INVISIBLE);
				progressBar.setProgress(100);
				kfulist = mPullRefreshListView.getRefreshableView();
				NewsListAdapter newsadapter = new NewsListAdapter(
						getActivity(), titlelist, photolist, textlist, datelist, true);
				kfulist.setAdapter(newsadapter);
				kfulist = mPullRefreshListView.getRefreshableView();
				registerForContextMenu(kfulist);
				if (sdb != null)
					sdb.delete("news", null, null);
				for (int i = 0; i < titlelist.size(); i++) {
					//add all news into database
					newValues.put(dbHelper.NEWS_COLUMN, titlelist.get(i));
					newValues.put(dbHelper.DET_TEXT_COLUMN, textlist.get(i));
					newValues.put(dbHelper.DATE_COLUMN, datelist.get(i));
					newValues
							.put(dbHelper.PHOTO_PATH_COLUMN, photo_path.get(i));
					// Вставляем данные в базу
					sdb.insert("news", null, newValues);
				}
				sdb.close();
				if (kfulist != null) {
					kfulist.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							Log.d("onItemClick", "position = " + position);
							// go to the new fragment to read news
							DetailNewsFragment det = new DetailNewsFragment(
									linklist.get(position - 1), titlelist
											.get(position - 1));
							FragmentTransaction transaction = getActivity()
									.getSupportFragmentManager()
									.beginTransaction();
							transaction.replace(R.id.news_fragment, det);
							transaction.addToBackStack(null);
							transaction.commit();
						}
					});
				}
			}
		}
	}
	
	/*
	 * if I want use this app in offline mode
	 * I need to download images into smartphone
	 */

	public String SaveImage(ArrayList<String> imagelist,
			ArrayList<String> photopath) {
		for (int i = 0; i < imagelist.size(); i++) {
			String filepath = "";
			if (imagelist.get(i).equals("11")) {
				photopath.add("11");
			} else {
				try {
					URL url = new URL(imagelist.get(i));
					HttpURLConnection urlConnection = (HttpURLConnection) url
							.openConnection();
					urlConnection.setRequestMethod("GET");
					urlConnection.setDoOutput(true);
					urlConnection.connect();
					File SDCardRoot = Environment.getExternalStorageDirectory()
							.getAbsoluteFile();
					String filename = i + ".png";
					Log.i("Local filename:", "" + filename);
					File file = new File(SDCardRoot, filename);
					if (file.createNewFile()) {
						file.createNewFile();
					}
					FileOutputStream fileOutput = new FileOutputStream(file);
					InputStream inputStream = urlConnection.getInputStream();
					int totalSize = urlConnection.getContentLength();
					int downloadedSize = 0;
					byte[] buffer = new byte[1024];
					int bufferLength = 0;
					while ((bufferLength = inputStream.read(buffer)) > 0) {
						fileOutput.write(buffer, 0, bufferLength);
						downloadedSize += bufferLength;
						Log.i("Progress:", "downloadedSize:" + downloadedSize
								+ "totalSize:" + totalSize);
					}
					fileOutput.close();
					if (downloadedSize == totalSize)
						filepath = file.getPath();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					filepath = null;
					e.printStackTrace();
				}
				photopath.add(filepath);
				Log.i("filepath:", " " + filepath);
			}
		}
		return null;
	}
}
