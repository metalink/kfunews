package ru.fosslabs.kfunews.fragments;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.squareup.picasso.Picasso;

import ru.fosslabs.kfunews.R;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailNewsFragment extends Fragment {
	TextView det_date;
	TextView det_title;
	TextView det_text;
	ImageView detImage;
	private String mUrl; 
	private String mTitle;
	FragmentActivity activity;

	public DetailNewsFragment(String mUrl, String mTitle) {
		this.mUrl = mUrl;
		this.mTitle = mTitle;
	}

	private Map<String, String> getConnectionDetails() {
		Map<String, String> networkDetails = new HashMap<String, String>();
		try {
			ConnectivityManager connectivityManager = (ConnectivityManager) getActivity()
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo wifiNetwork = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (wifiNetwork != null && wifiNetwork.isConnected()) {

				networkDetails.put("Type", wifiNetwork.getTypeName());
				networkDetails.put("Sub type", wifiNetwork.getSubtypeName());
				networkDetails.put("State", wifiNetwork.getState().name());
			}

			NetworkInfo mobileNetwork = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (mobileNetwork != null && mobileNetwork.isConnected()) {
				networkDetails.put("Type", mobileNetwork.getTypeName());
				networkDetails.put("Sub type", mobileNetwork.getSubtypeName());
				networkDetails.put("State", mobileNetwork.getState().name());
				if (mobileNetwork.isRoaming()) {
					networkDetails.put("Roming", "YES");
				} else {
					networkDetails.put("Roming", "NO");
				}
			}
		} catch (Exception e) {
			networkDetails.put("Status", e.getMessage());
		}
		return networkDetails;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.detail_news, container, false);
		setHasOptionsMenu(true);
		det_date = (TextView) view.findViewById(R.id.det_date);
		det_title = (TextView) view.findViewById(R.id.det_title);
		det_text = (TextView) view.findViewById(R.id.det_text);
		detImage = (ImageView) view.findViewById(R.id.detImage);
 		Log.d("FRAGMENT", "is working detail");
		Map<String, String> networkDetails = getConnectionDetails();
		if (networkDetails.isEmpty()) {
			Toast.makeText(getActivity().getApplicationContext(),
					"Нет интернет подключения", Toast.LENGTH_LONG).show();
		}
		new getDetailNews().execute();
		return view;
	}

	class getDetailNews extends AsyncTask<Void, Void, String> {
		private String date;
		private String text;
		private String image;
		
		@Override
		protected String doInBackground(Void... params) {
			Document doc = null;
			try {
				doc = Jsoup.connect(
						mUrl).get();
				Elements elems1 = doc.select("div");
				for (Element elem : elems1) {
					if (elem.attr("align").equals("justify")) {
						text = elem.toString();
					}
				}
				Elements elems = doc.select("div");
				for (Element elem : elems) {
					if (elem.attr("class").equals("vn_new_date")) {
						date = elem.text();
					}
				}
				Elements elems3 = doc.select("img");
				for (Element elem : elems3) {
					if (elem.attr("style").equals("min-height:1px;")) {
						image = "http://kpfu.ru" + elem.attr("src");
						Log.d("Parser", "Photo: " + elem.attr("src"));
					} 
				}
//				Log.d("DetParser", "Parse = " + date + " " + title + " " + text);
			} catch (Exception e) {
				
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			det_date.setText(date);
			det_text.setText(Html.fromHtml(text));
			det_title.setText(mTitle);
			Picasso.with(getActivity()).load(image).into(detImage);
			super.onPostExecute(result);
		}
	}
	
}
