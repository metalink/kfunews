package ru.fosslabs.kfunews.adapter;

import java.io.File;
import java.util.ArrayList;

import ru.fosslabs.kfunews.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class NewsListAdapter extends BaseAdapter{
	
	Activity context;
	ArrayList<String> text;
	ArrayList<String> photo;
	ArrayList<String> dettext;
	ArrayList<String> date;
	boolean connection;
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return text.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return text.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return text.get(position).hashCode();
	}

	public NewsListAdapter(Activity context, ArrayList<String> text,
			ArrayList<String> photo, ArrayList<String> dettext,
			ArrayList<String> date, boolean connection) {
		super();
		this.context = context;
		this.text = text;
		this.photo = photo;
		this.dettext = dettext;
		this.date = date;
		this.connection = connection;
	}
	
	private class ViewHolder {
		TextView txtView;
		ImageView photo1;
		TextView detView;
		TextView tx_date;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.layout_newsadapter, null);
			holder = new ViewHolder();
			holder.tx_date = (TextView) convertView
					.findViewById(R.id.date);
			holder.txtView = (TextView) convertView.findViewById(R.id.title);
			holder.detView = (TextView) convertView
					.findViewById(R.id.text);
			holder.photo1 = (ImageView) convertView.findViewById(R.id.imageView1);
			if (connection) {
			if (!photo.get(position).equals("11")) {
				Picasso.with(context).load(photo.get(position)).into(holder.photo1);
			} else {
				holder.photo1.setBackgroundResource(android.R.drawable.btn_star);
			}
			} else {
				File f = new File(photo.get(position));
				Bitmap bmp = BitmapFactory.decodeFile(f.getAbsolutePath());
				holder.photo1.setImageBitmap(bmp);
			}
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.txtView.setText(text.get(position));
		holder.detView.setText(dettext.get(position));
		holder.tx_date.setText(date.get(position));	
		return convertView;
	}

}
