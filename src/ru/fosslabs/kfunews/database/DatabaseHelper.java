package ru.fosslabs.kfunews.database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper implements BaseColumns {

	private static final String DATABASE_NAME = "newsdatabase.db";
	private static final String DATABASE_TABLE = "news";

	public static final String DATE_COLUMN = "date"; //column name "date"
	public static final String NEWS_COLUMN = "news"; //column name "news"
	public static final String DET_TEXT_COLUMN = "text"; //column name "text"
	public static final String PHOTO_PATH_COLUMN = "photo_path"; //column name "photo_path"

	private static final String DATABASE_CREATE_SCRIPT = "create table "
			+ DATABASE_TABLE + " (" + BaseColumns._ID
			+ " integer primary key autoincrement, " + DATE_COLUMN
			+ " text not null, " + NEWS_COLUMN + " text not null, " + DET_TEXT_COLUMN
			+ " text not null,"+ PHOTO_PATH_COLUMN +" text not null);";

	public DatabaseHelper(Context context, String name, CursorFactory factory,
			int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
		// TODO Auto-generated constructor stub
	}

	public DatabaseHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(DATABASE_CREATE_SCRIPT);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		// Запишем в журнал
		Log.w("SQLite", "Обновляемся с версии " + oldVersion + " на версию " + newVersion);
		
		// Удаляем старую таблицу и создаём новую
		db.execSQL("DROP TABLE IF IT EXIST " + DATABASE_TABLE);
		// Создаём новую таблицу
		onCreate(db);
	}
}